#!/bin/bash
# Version 2022.03.04

# This program will take input from a file. It will read the list of files that need to have the 
# original file put back into place. It will verify the backup file exists, remove the remediated file
# and then place the original file back into place. I am using the same file that we used to remediate
# so that you can easily just feed it the file and undo a remediation if necessary.
#
# Isaac Davis
# isaac.davis@tea.texas.gov
# 03/04/2022

USAGE="\n\nUsage: $0 <inputfile> \n\n  You must specify an input file for this script to run. The file will usually be the same file that you used to remediate.\n\n"


#Check to make sure a filename is passed into the script.
if [ "$#" == "0" ]; then
    printf "$USAGE"
    exit 1
fi

# Check to make sure the input file exists.
if [ ! -f $1 ]; then
    printf "\n\nERROR: The input file $1 does not exist. Exiting the script.\n\n"
    exit 1
fi


# Start the main loop. We will go through the input file checking to make sure each file exists and then restoring the original files.
while read line
do
	# This checks to see if the file exists and also that the line isn't empty. We want to skip empty lines and entries that do no exist
	if [ -f $line ] && [ "$line" != "" ]; then
		backup_file=$line.backup.log4jremediation
		printf "Restoring $backup_file to $line.\n"
		mv "$backup_file" "$line"
	else
		# We want to warn the user if they have listed a file that does not exist on the machine. I had a machine
		# that evidently was putting in a log4j jar file in a temporary folder, and when I shut down the services
		# the jar file was removed. 
		printf "WARNING - The file \"$line\" does not exist.\n"
        fi
done < $1

#!/bin/bash

# This script will run all of the current log4j scripts for log4j files and embedded log4j files. 
# It will then verify and run the remediation scripts. It will prompt along the way.
# Isaac Davis 04/04/2022

log4jfilenumber=0
embeddedfilenumber=0



# Confirm routine to make sure if the user wants to continue
vrify() #@ DESCRIPTION: Ask for a (y/n) answer and process the results..
{
	while true
        do
                printf "$1\n(Y/N)?"
                read prompt
                case "$prompt" in
                        [Yy]) break ;;
                        [Nn]) printf "\n\nExiting the script.\n\n"
                        	exit ;;
                        *) echo "Please answer Y or N" ;;
                esac
        done
}

# Main block of code.

printf "\n\nFirst step is to make sure all your applications are stopped.\n"
vrify "Are your applications stopped?"

printf "\n\nWe are going to first check to see what files need to be remediated.\n"

printf "Checking for log4j files. This may take a few minutes.\n"
sudo ./log4j_version.sh 2>/dev/null > beforeremediation_log4j.csv

printf "Checking for embedded log4j files. This may take a few minutes.\n"
sudo ./log4j_ear_version.sh 2>/dev/null > beforeremediation_ear.csv

printf "Determining log4j files that need remediation.\n"
cat beforeremediation_log4j.csv | grep Yes | awk -F, '{print $2}' | sort -fu > files2remediate_log4j.txt

printf "Determining embedded log4j files that need remediation.\n"
cat beforeremediation_ear.csv | grep Yes | awk -F, '{print $2}' | sort -fu > files2remediate_ear.txt

printf "\n\n"

log4jfilenumber=`cat files2remediate_log4j.txt | wc -l`
if [ "$log4jfilenumber" = "0" ]; then
	printf "There are NO standalone log4j files to remediate on this machine.\n"
else
	printf "\n\nThere are `cat files2remediate_log4j.txt | wc -l` Log4j files with `cat beforeremediation_log4j.csv | grep Yes | wc -l` vulnerablilites found to remediate:\n===============================================================================\n"
	cat files2remediate_log4j.txt
	printf "\n"
	vrify "Does this list look ok?"
	
	printf "\nWe are going to remediate the standalone log4j files.\n"
	vrify "Ready?"
	
	printf "\n\nBeginning remediation of standalone log4j files.\n"
	# UNCOMMENT THESE TO ACTUALLY ALLOW REMEDIATION
	sudo ./remediate_log4j.sh files2remediate_log4j.txt > remediation_log4j.out
	printf "Preliminary check, there are `cat remediation_log4j.out | grep 'ERROR: ' | wc -l` errors in the remediation_log4j.out file.\n\n"
	
	printf "Re-running the version information script.\n"
	sudo ./log4j_version.sh 2>/dev/null > afterremediation_log4j.csv

	printf "\n\nThere are `cat afterremediation_log4j.csv | grep Yes | wc -l` vulnerabilities after standalone log4j remediation.\n"

fi

embeddedfilenumber=`cat files2remediate_ear.txt | wc -l`
if [ "$embeddedfilenumber" = "0" ]; then
	printf "There are NO embedded log4j files to remediate on this machine.\n"
else
	printf "\n\nThere are `cat files2remediate_ear.txt | wc -l` file(s) with embedded log4j files that have `cat beforeremediation_ear.csv | grep Yes | wc -l` vulnerabilities that need to be remediated:\n=============================================================\n"
	cat files2remediate_ear.txt
	printf "\n"
	vrify "Does this list look ok?"
	
	printf "\nWe are going to remediate the embedded log4j files.\n"
	vrify "Ready?"
	
	printf "\n\nBeginning remediation of embedded log4j files.\n"
	# UNCOMMENT THESE TO ACTUALLY ALLOW REMEDIATION
	sudo ./remediate_ear_log4j.sh files2remediate_ear.txt > remediation_log4j_ear.out
	printf "Preliminary check, there are `cat remediation_log4j_ear.out | grep 'ERROR: ' | wc -l` errors in the remediation_log4j_ear.out file.\n\n"
	
	printf "Re-running the version information script.\n"
	sudo ./log4j_ear_version.sh 2>/dev/null > afterremediation_ear.csv

	printf "\n\nThere are `cat afterremediation_ear.csv | grep Yes | wc -l` vulnerabilities in embedded log4j files after remediation.\n"

fi

printf "\n\nRemediation complete.\n\n"


#!/bin/bash

# Linux Version

# EAR file version - Remediates log4j files embedded in .ear, .war, and .jar files.

# This program will take input from a file. It will read the list of files that are to be remediated.
# Then it will verify the file exists, make a backup and then remediate the embedded log4j*.jar file inside.
# 
# Isaac Davis
# isaac.davis@tea.texas.gov

# List of CVE's we are remediating against
#CVE-2020-9488 - SMTPAppender - https://nvd.nist.gov/vuln/detail/CVE-2020-9484
#CVE-2022-23307 - Apache Chainsaw - https://nvd.nist.gov/vuln/detail/CVE-2022-23307
#CVE-2022-23305 - JDBCAppender - https://nvd.nist.gov/vuln/detail/CVE-2022-23305
#CVE-2022-23302 - JMSSink - https://nvd.nist.gov/vuln/detail/CVE-2022-23302
#CVE-2019-17571 - SocketServer, JMSAppender - https://nvd.nist.gov/vuln/detail/CVE-2019-17571
#CVE-2021-45046 - JNDILookup - https://nvd.nist.gov/vuln/detail/CVE-2021-45046
#CVE-2021-4104 - JMSAppender - https://nvd.nist.gov/vuln/detail/CVE-2021-4104
#CVE-2021-44832 - JDBCAppender - https://nvd.nist.gov/vuln/detail/CVE-2021-44832
#CVE-2021-44228 - JNDILookup - https://nvd.nist.gov/vuln/detail/CVE-2021-44228


USAGE="\n\nUsage: $0 <inputfile> \n\n  You must specify an input file for this script to run. The file needs to have the files to remediate in a single line with a full path.\n\n"

TEMPFOLDER="/tmp/log4jembedded"

# Set up the list of classes we are removing from the .jar files
# REMOVECLASS - Separate the classes with a space. Used in the zip command.
REMOVECLASS="org/apache/logging/log4j/core/lookup/JndiLookup.class org/apache/log4j/jdbc/JDBCAppender.class org/apache/logging/log4j/core/appender/db/jdbc/JdbcAppender.class org/apache/log4j/net/JMSSink.class org/apache/log4j/chainsaw/* org/apache/log4j/net/SocketServer.class org/apache/log4j/net/JMSAppender.class org/apache/log4j/net/SMTPAppender.class"

#SEARCHCLASS - Separate the classes with a |. Used in the grep -E command to find classes in the jar file.
SEARCHCLASS="org/apache/logging/log4j/core/lookup/JndiLookup.class|org/apache/log4j/jdbc/JDBCAppender.class|org/apache/logging/log4j/core/appender/db/jdbc/JdbcAppender.class|org/apache/log4j/net/JMSSink.class|org/apache/log4j/chainsaw|org/apache/log4j/net/SocketServer.class|org/apache/log4j/net/JMSAppender.class|org.apache.log4j.net.SMTPAppender.class"



#Check to make sure a filename is passed into the script.
if [ "$#" == "0" ]; then
       printf "$USAGE"
       exit 1
fi

# Check to make sure the input file exists.
if [ ! -f $1 ]; then
    printf "\n\nERROR: The input file $1 does not exist. Exiting the script.\n\n"
    exit 1
fi

# Had a machine that did not have zip installed. This is a required program on the machine to perform the remediation.
# This block of code checks to see if it's available to the users
which zip 1>/dev/null 2>/dev/null > /dev/null
if [ `echo $?` != "0" ]; then
    printf "\n\nERROR: The zip program is not available. Zip is required to remediate log4j files. Stopping the script.\n\n"
    exit 1
fi



# Start the main loop. We will go through the input file checking to make sure each file exists and then remediating.



while read line
do
	# Timestamp
	printf "=====> Beginning work on $line - `date`\n"
	# Lets make sure the temp folder doesn't exist. If it does, we need to remove it.
	if [ -d "$TEMPFOLDER" ]; then
	   rm -rf $TEMPFOLDER
	fi
	
	# Make the temp folder.
	mkdir $TEMPFOLDER
	
	# This checks to see if the file exists and also that the line isn't empty. We want to skip empty lines and entries that do no exist
	if [ -f "$line" ] && [ "$line" != "" ]; then
		    # First we need to find the owner of the file. 
		    file_owner="`ls -l "$line" | awk '{print $3}'`"

		    # We need to make sure that the zip program is available. Had a machine where zip was not in the path,
		    # and not available for the owner of the file. This caused an issue since the remediation command
		    # cannot be run.
		    sudo su -c "which zip" $file_owner
		    if [ `echo $?` != "0" ]; then
			   printf "\n\nERROR: The zip program is not available for \"$file_owner\". Stopping the script.\n\n"
			   exit 1
	        fi
		     
		    # If zip is available and the file exists, start the backup process.
		    # This is the default backup name.  
		    backup_file="$line".backup.log4jremediation
		    
		    # Checking to see if the backup file already exists. We do not want to overwrite the file in case the script
		    # is being run more than once for some reason. This will check if it exists. If it does, then give the backup 
		    # file a name with the date and time. EG: /tmp/log4j.jar.backup.log4jremediation.2022_0217_13_45_36
		    if [ -f "$backup_file" ]
	            then
			    backup_file="$line".backup.log4jremediation.`date +%Y_%m%d_%H_%M_%S`
	        fi

		    # Making the backup of the log4j jar file.
	        printf "We are backing up and remediating the following file: "$line"\n"
		    printf "Copying \"$line\" to \"$backup_file\"\n"
		    cp -p "$line" "$backup_file"

		    # Make sure the backup file exists before touching the original file.
		    if [ ! -f "$backup_file" ]; then
			# If the backup file doesn't exist, we need to exit. Could be a space issue, or permissions issue.
			printf "ERROR: Could not make backup - \"$backup_file\" \n"
		    	printf "ERROR: The backup did NOT work. Exiting the script. Make sure $file_owner has permissions to this folder.\n"
			exit 1
		    fi
		    
			# =======
			# Main unzip block
			
					# Had an issue where the owner of the file was a number and it caused an error when trying to use that
					# userid. "ERROR: The zip program is not available for 775. Stopping the script." The new plan is to change
					# the file ownership to root, remediate, and then change it back to the original owner.
					# Time to remove the class from the zip file after swapping owners.
					printf "=====> Remediating classes from \"$line\" as user $file_owner.\n"
					chown root "$line"
					
					# Unzip the file
					printf "=====> Unzipping \"$line\"\n"
					cd $TEMPFOLDER
					unzip -o "$line"
					
					# Find all log4j jar files in the unzipped ear file
					find . -name 'log4j*.jar' -print > /tmp/currentlog4j_internalfiles.out
					
					# Go into each log4j.jar file and remove any of the classes that need to be remediated.
					while read log4jfile
					do
						cd $TEMPFOLDER
						# Check to make sure the jar file contains the JndiLookup.class
						if [ `unzip -t "$log4jfile" | grep -E "$SEARCHCLASS" | wc -l` != 0 ]; then
							printf "====> Removing classes from \"$log4jfile\".\n"
							zip -q -d "$log4jfile" $REMOVECLASS
						else
							# If there are no classes in the jar file to remediate, let the user know.
							printf "There are no classes to remediate in \"$log4jfile\"\n"
						fi

						if [ `unzip -t "$log4jfile" | grep -E  "$SEARCHCLASS" | wc -l` == 0 ]; then
							printf "====> Remediated \"$log4jfile\"\n\n"
						else
							printf "ERROR: A class file still exists in \"$log4jfile\". Something went wrong. Please investigate.\n"
						fi
					done < /tmp/currentlog4j_internalfiles.out

					rm -f "$line"

					printf "=====> Zipping up $line .\n"
					cd $TEMPFOLDER
					zip -r "$line" *
					
			# =======
			# Things should be zipped back up by this point.
			
		    chmod --reference="$backup_file" "$line"
		    chown --reference="$backup_file" "$line"

		    # We go back to the jar file, and double check that the JndiLookup.class has been removed.
		    # If not, alert the user and stop the script.
			
			# Cleanup.
			cd /tmp
			rm -rf $TEMPFOLDER
			rm -rf /tmp/log4j/currentlog4j.out

	else
		# We want to warn the user if they have listed a file that does not exist on the machine. I had a machine
		# that evidently was putting in a log4j jar file in a temporary folder, and when I shut down the services
		# the jar file was removed. 
		printf "WARNING - The file \"$line\" does not exist.\n"
    	fi

	# Timestamp
	printf "=====> Ending work on $line - `date`\n"

	# Put some space in the output.
	printf "\n\n"
done < $1


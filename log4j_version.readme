Instructions for log4j_version.sh
By Isaac Davis
Version 4/1/2022

Overview:
This Linux Bash script will find all files with the naming convention “log4j*.jar” on a machine. Once the list of files is compiled, 
it will take each jar file, unzip the META-INF/MANIFEST.MF file into a temp folder and then parse out the following fields: 
Implementation Title, Implementation Vendor, Implementation Version, and Log4jRelease Version. It is worth noting that some of the 
jar files will have some, all, or none of the fields in the MANIFEST.MF file. 

The script will export the output as csv (comma delimited) format which can then be imported into Excel or a DB. There is also 
a command line option to get the results in a report style format.

Operation:
To run the script, you need to copy the file over to your machine in a folder you have permissions. Make sure the file is executable 
(usually 744). I would suggest running this with sudo privileges, but if you aren’t comfortable with that it should work on most of 
the files depending on the .jar’s permissions. As long as you can read them all, the script will work.

sudo ./log4j_version.sh 

If you want to capture the output to a file, you can simply redirect the output to a file:

./log4j_version.sh > /tmp/machinename_log4jversions.csv

Once you have the formatted output you can then import this into a spreadsheet or db. I capture the following fields in the csv output:

Machine name
log4j file
md5sum
Implementation Title
Implementation Vendor
Implementation Version
Log4jRelease Version
CVE
CVE Description
Status
Owner
Group
Permissions
Backup File
Backup Date
Date scanned
Time scanned


The md5sum is a checksum value that is used to determine if files are identical. I am using it in this case solely to identify multiple copies of the same file that exist on a machine. What I am finding is that they are sometimes named differently, but are identical. This will also alert us to when a jar file has been modified.

Output (this is an example of the csv output):
Machine name,log4j file,md5sum,Implementation Title,Implementation Vendor,Implementation Version,Log4jRelease Version,CVE,CVE Description,Status,Owner,Group,Permissions,Backup File
,Backup Date,Date scanned,Time scanned
tea4svmurphy,/opt/apps/tcaptst/appserv/classes/log4j-core.jar,165315a0dfaef18450a21fbd3ba1dd4b, Apache Log4j Core, The Apache Software Foundation,"2.13.3","2.13.3",CVE-2021-44228,A
pache Log4j < 2.15.0 Remote Code Execution (Nix),No,tcaptst,sotadmin,-rwxrwxrwx,/opt/apps/tcaptst/appserv/classes/log4j-core.jar.backup.log4jremediation,Mar 23 10:10,2022_0401,12:2
9:45
tea4svmurphy,/opt/apps/tcaptst/appserv/classes/log4j-core.jar,165315a0dfaef18450a21fbd3ba1dd4b, Apache Log4j Core, The Apache Software Foundation,"2.13.3","2.13.3",CVE-2021-45046,A
pache Log4j 2.x < 2.16.0 RCE,No,tcaptst,sotadmin,-rwxrwxrwx,/opt/apps/tcaptst/appserv/classes/log4j-core.jar.backup.log4jremediation,Mar 23 10:10,2022_0401,12:29:45
tea4svmurphy,/opt/apps/tcaptst/appserv/classes/log4j-core.jar,165315a0dfaef18450a21fbd3ba1dd4b, Apache Log4j Core, The Apache Software Foundation,"2.13.3","2.13.3",CVE-2021-44832,A
pache Log4j 2.0 < 2.3.2 / 2.4 < 2.12.4 / 2.13 < 2.17.1,No,tcaptst,sotadmin,-rwxrwxrwx,/opt/apps/tcaptst/appserv/classes/log4j-core.jar.backup.log4jremediation,Mar 23 10:10,2022_040
1,12:29:45

Once you have the output, you can then import this into a spreadsheet, or a database.

Any questions or comments you can reach me via email:
Isaac.davis@tea.texas.gov

Changelog: 
12/22/2021 – Added in code to check for and report if the JndiLookup.class file is contained in the jar file
02/10/2022 - Changed the default output of the script. Added in file owner, group, and permissions
03/04/2022 - Script has been tested on a number of machines by the Tools team.
04/01/2022 - Updates to the readme. Corrected fields.

Thanks,
Isaac Davis
12/21/2021


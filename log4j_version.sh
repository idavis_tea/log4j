#!/bin/bash

# Linux Version

# This program will find log4j*.jar files. Print out a checksum of the files, and then go unzip the MANIFEST.MF file
# for each one and print out the version number.
# Added in code to output the results in csv format so we can import into Excel.
# Isaac Davis 12/20/2021
# isaac.davis@tea.texas.gov

# 12/22/2021 - Added in the search for the JndiLookup.class inside the jar file.
# 02/10/2022 - Changed csv to default output and added in three fields: Owner, Group, and Permissions
# 03/16/2022 - Lots of changes. Removing report style output as I don't believe it is on any help.


# Setup the working folder and temporary subdir used to unpack the jar files.
cd /tmp
if [ ! -d l4tmp ]; then
    mkdir l4tmp
fi

# Get rid of the l4temp.out file in case it was left around. I overwrite it in the unzip command but am doing
# this just to be safe.
if [ -f l4temp.out ]; then
	rm l4temp.out
fi

USAGE="\n\nUsage: $0 \n\n"


# This is a subroutine to check for a particular package in the jar file. Making this a subroutine since there
# are multiple class files to check for.
pcheck() #@ DESCRIPTION: Print out Yes or No if the package passed is found.
{
	# Start with the machine name.
	printf "`uname -n`,"
	# Some of the Manifest files contain Windows cr/lf chars (eeek), so this strips them out. 
	sed -i 's///g' META-INF/MANIFEST.MF
	printf "$line,"
	OPUT=`md5sum $line | tr -d '\n' | awk '{print $1}'`
	printf "${OPUT},"
	
	OPUT=`grep -Z -i 'Implementation-Title:' META-INF/MANIFEST.MF | awk -F: '{print $2}' | tr -d '\n'`
	printf "${OPUT},"
	
	OPUT=`grep -Z -i 'Implementation-Vendor:' META-INF/MANIFEST.MF|  awk -F: '{print $2}' | tr -d '\n'`
	printf "${OPUT},"
	
	OPUT="\"`grep -Z -i 'Implementation-Version:' META-INF/MANIFEST.MF|  awk -F: '{print $2}' | tr -d '\n' | sed -E 's/ //g'`\""
	printf "${OPUT},"
	
	OPUT="\"`grep -Z -i 'Log4jReleaseVersion:' META-INF/MANIFEST.MF|  awk -F: '{print $2}' | tr -d '\n' | sed -E 's/ //g'`\""
	printf "${OPUT},"

	printf "$1,$2,"
        if [ `unzip -t $line | grep $3 | wc -l` != 0 ]; then
                printf "Yes,"
        else
                printf "No,"
        fi

	# This prints out the Owner, Group, and permissions of the file.
	OPUT=`ls -l $line | awk '{print $3","$4","$1}'`
	printf "${OPUT},"
	
	if [ -f "$line.backup.log4jremediation" ]; then
		printf "$line.backup.log4jremediation,"
		# We grab the date/time of the remediated file since the backup will have the original info.
		OPUT=`ls -l $line | awk '{print $6" "$7" "$8}'`
		printf "$OPUT,"
	else
		printf ",,"
	fi

	# Lastly we put a timestamp in the results so we know when the check was performed.
	printf "`date +%Y_%m%d,%H:%M:%S`\n"	
		
}


# This block of code will find the files, unpack them, and output everything in a csv format (comma delimited). 
# Changed this to be the default mode.
# Note: The md5sum can be useful to determine if the files are identical
# Print out the first line of the csv output. This will be used as the header when importing into excel or libreoffice.
printf "Machine name,log4j file,md5sum,Implementation Title,Implementation Vendor,Implementation Version,Log4jRelease Version,CVE,CVE Description,Status,Owner,Group,Permissions,Backup File,Backup Date,Date scanned,Time scanned\n"
#printf "Machine name,log4j file,md5sum,Implementation Title,Implementation Vendor,Implementation Version,Log4jRelease Version,CVE,CVE Description,Status,Owner,Group,Permissions,Date scanned,Time scanned\n"

# I first build the list of files and put them in a temp file: /tmp/l4temp.out
find / -type d -name mnt -prune -o -name 'log4j*.jar' -exec md5sum {} \; 2>/dev/null | sort | awk '{print $2}' > /tmp/l4temp.out

# Temp file is created. Now use it as a loop to go through each file and get information.
while read line
do
	
	cd /tmp/l4tmp
	unzip -o $line META-INF/MANIFEST.MF 1>/dev/null 2>/dev/null
	
	# This will check to see if the .jar file contains a JndiLookup.class file and add that to the column results.
	# Note: added in more classes to look for and turned the check into a subroutine.
	pcheck "CVE-2021-44228" "Apache Log4j < 2.15.0 Remote Code Execution (Nix)" "org/apache/logging/log4j/core/lookup/JndiLookup.class"
	pcheck "CVE-2021-45046" "Apache Log4j 2.x < 2.16.0 RCE" "org/apache/logging/log4j/core/lookup/JndiLookup.class"
	pcheck "CVE-2021-44832" "Apache Log4j 2.0 < 2.3.2 / 2.4 < 2.12.4 / 2.13 < 2.17.1" "org/apache/log4j/jdbc/JDBCAppender.class"
	pcheck "CVE-2022-23305" "Apache Log4j 1.x Multiple Vulnerabilities" "org/apache/logging/log4j/core/appender/db/jdbc/JdbcAppender.class"
	pcheck "CVE-2022-23302" "Apache Log4j 1.x Multiple Vulnerabilities" "org/apache/log4j/net/JMSSink.class"
	pcheck "CVE-2022-23307" "Apache Log4j 1.x Multiple Vulnerabilities" "org/apache/log4j/chainsaw"
	pcheck "CVE-2019-17571" "Apache Log4j 1.x Multiple Vulnerabilities" "org/apache/log4j/net/SocketServer.class"
	pcheck "CVE-2021-4104" "Apache Log4j 1.2 JMSAppender Remote Code Execution" "org/apache/log4j/net/JMSAppender.class"
	pcheck "CVE-2020-9488" "Apache Log4j 1.x Multiple Vulnerabilities" "org/apache/log4j/net/SMTPAppender.class"
	
	# Cleanup the current unzipped MANIFEST file.
	rm -rf META-INF
	
done < /tmp/l4temp.out


# Cleanup files.
cd /tmp
rm -rf l4tmp
rm l4temp.out


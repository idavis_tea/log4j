#!/bin/bash

mprocess() #@ DESCRIPTION: Process each machine.
{
	printf "Working on $2.\n"
	scp /home/idavis/src/log4j/log4j_version.sh $1@$2:/home/$1/src
	scp /home/idavis/src/log4j/remediate_log4j.sh $1@$2:/home/$1/src
	scp /home/idavis/src/log4j/reverse_remediation_log4j.sh $1@$2:/home/$1/src
	ssh $1@$2 "if [ -f /tmp/idtemp ]; then rm /tmp/idtemp; fi"
	ssh -t $1@$2 "sudo /home/$1/src/log4j_version.sh > /tmp/idtemp"
	scp $1@$2:/tmp/idtemp ./$2_log4j.out
	printf "\n"
}

printf "Working on tea4svsasdev.\n"
sudo /home/idavis/src/log4j/log4j_version.sh > tea4svsasdev_log4j.out

mprocess "idavis" "tea4svsastst"
mprocess "idavis" "tea4avsasint"
mprocess "idavis" "tea4avsasext"
mprocess "idavis" "tea4svdsdev"
mprocess "idavis" "tea4svdstst"
mprocess "idavis" "tea4avdsprd"
mprocess "idavis" "tea4svmurphy"
mprocess "idavis" "tea4avhouston"
mprocess "isaacdavis" "tea4svtrigger"
mprocess "isaacdavis" "tea4svcondor"
mprocess "isaacdavis" "tea4avtower"
mprocess "idavis" "tea4svtools8"
mprocess "idavis" "tea4svtools10"
mprocess "idavis" "tea4svbonito"

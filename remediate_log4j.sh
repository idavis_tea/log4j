#!/bin/bash

# Linux Version

# This program will take input from a file. It will read the list of files that are to be remediated.
# Then it will verify the file exists, make a backup and then run the command to remove the
# JndiLookup.class, and other classes from the file. 
#
# The command is: zip -q -d path-to-JAR-file org/apache/logging/log4j/core/lookup/JndiLookup.class
# that is the example from the SAS remediation page: 
# https://go.documentation.sas.com/doc/en/log4j/1.0/p1gaeukqxgohkin1uho5gh7v5s7p.htm#p1qtxa6bxxcy4in1a2s7iso3adi2
#
# Isaac Davis
# isaac.davis@tea.texas.gov

# List of CVE's we are remediating against
#CVE-2020-9488 - SMTPAppender - https://nvd.nist.gov/vuln/detail/CVE-2020-9484
#CVE-2022-23307 - Apache Chainsaw - https://nvd.nist.gov/vuln/detail/CVE-2022-23307
#CVE-2022-23305 - JDBCAppender - https://nvd.nist.gov/vuln/detail/CVE-2022-23305
#CVE-2022-23302 - JMSSink - https://nvd.nist.gov/vuln/detail/CVE-2022-23302
#CVE-2019-17571 - SocketServer, JMSAppender - https://nvd.nist.gov/vuln/detail/CVE-2019-17571
#CVE-2021-45046 - JNDILookup - https://nvd.nist.gov/vuln/detail/CVE-2021-45046
#CVE-2021-4104 - JMSAppender - https://nvd.nist.gov/vuln/detail/CVE-2021-4104
#CVE-2021-44832 - JDBCAppender - https://nvd.nist.gov/vuln/detail/CVE-2021-44832
#CVE-2021-44228 - JNDILookup - https://nvd.nist.gov/vuln/detail/CVE-2021-44228


USAGE="\n\nUsage: $0 <inputfile> \n\n  You must specify an input file for this script to run. The file needs to have the files to remediate in a single line with a full path.\n\n"


# Set up the list of classes we are removing from the .jar files
# REMOVECLASS - Separate the classes with a space. Used in the zip command.
REMOVECLASS="org/apache/logging/log4j/core/lookup/JndiLookup.class org/apache/log4j/jdbc/JDBCAppender.class org/apache/logging/log4j/core/appender/db/jdbc/JdbcAppender.class org/apache/log4j/net/JMSSink.class org/apache/log4j/chainsaw/* org/apache/log4j/net/SocketServer.class org/apache/log4j/net/JMSAppender.class org/apache/log4j/net/SMTPAppender.class"

#SEARCHCLASS - Separate the classes with a |. Used in the grep -E command to find classes in the jar file.
SEARCHCLASS="org/apache/logging/log4j/core/lookup/JndiLookup.class|org/apache/log4j/jdbc/JDBCAppender.class|org/apache/logging/log4j/core/appender/db/jdbc/JdbcAppender.class|org/apache/log4j/net/JMSSink.class|org/apache/log4j/chainsaw|org/apache/log4j/net/SocketServer.class|org/apache/log4j/net/JMSAppender.class|org.apache.log4j.net.SMTPAppender.class"


#Check to make sure a filename is passed into the script.
if [ "$#" == "0" ]; then
       printf "$USAGE"
       exit 1
fi

# Check to make sure the input file exists.
if [ ! -f $1 ]; then
    printf "\n\nERROR: The input file $1 does not exist. Exiting the script.\n\n"
    exit 1
fi

# Had a machine that did not have zip installed. This is a required program on the machine to perform the remediation.
# This block of code checks to see if it's available to the users
which zip 1>/dev/null 2>/dev/null > /dev/null
if [ `echo $?` != "0" ]; then
    printf "\n\nERROR: The zip program is not available. Zip is required to remediate log4j files. Stopping the script.\n\n"
    exit 1
fi



# Start the main loop. We will go through the input file checking to make sure each file exists and then remediating.
while read line
do
	# This checks to see if the file exists and also that the line isn't empty. We want to skip empty lines and entries that do no exist
	if [ -f $line ] && [ "$line" != "" ]
	then
		# Check to make sure the jar file contains the JndiLookup.class
		if [ `unzip -t $line | grep -E "$SEARCHCLASS" | wc -l` != 0 ] 
		then 
			# First we need to find the owner of the file. We want to run the cp and zip commands as this user.
		    	file_owner="`ls -l $line | awk '{print $3}'`"

		     
		    	# If zip is available and the file exists, start the backup process.
		    	# This is the default backup name.  
		    	backup_file=$line.backup.log4jremediation
		    
		    	# Checking to see if the backup file already exists. We do not want to overwrite the file in case the script
		    	# is being run more than once for some reason. This will check if it exists. If it does, then give the backup 
		    	# file a name with the date and time. EG: /tmp/log4j.jar.backup.log4jremediation.2022_0217_13_45_36
		    	if [ -f $backup_file ]
	            	then
				  backup_file=$line.backup.log4jremediation.`date +%Y_%m%d_%H_%M_%S`
	        	fi

				# Making the backup of the log4j jar file.
					printf "We are backing up and remediating the following file: $line\n"
				printf "Copying $line to $backup_file.\n"
				cp -p $line $backup_file

				# Make sure the backup file exists before touching the original file.
				if [ ! -f $backup_file ]; then
					# If the backup file doesn't exist, we need to exit. Could be a space issue, or permissions issue.
					printf "ERROR: Could not make backup - $backup_file \n"
					printf "ERROR: The backup did NOT work. Exiting the script. Make sure $file_owner has permissions to this folder.\n"
					exit 1
				fi
		
				# Had an issue where the owner of the file was a number and it caused an error when trying to use that
				# userid. "ERROR: The zip program is not available for 775. Stopping the script." The new plan is to change
				# the file ownership to root, remediate, and then change it back to the original owner.
				# Time to remove the class from the zip file after swapping owners.
				printf "Remediating classes from $line.\n"
				chown root $line
				zip -q -d $line $REMOVECLASS
				chown --reference=$backup_file $line

				# Change the permissions of the modified file back to the originals permissions.
				chmod --reference=$backup_file $line

				# We go back to the jar file, and double check that the JndiLookup.class has been removed.
				# If not, alert the user and stop the script.
				if [ `unzip -t $line | grep -E  "$SEARCHCLASS" | wc -l` == 0 ]; then
					printf "Remediated $line\n\n"
					else
					printf "ERROR: A class file still exists in $line. Something went wrong. Please investigate. Make sure $file_owner has permission to this folder.\n"
				fi
		else
		    # If there are no classes in the jar file to remediate, let the user know.
		    printf "There are no classes to remediate in $line\n"
		fi
	else
		# We want to warn the user if they have listed a file that does not exist on the machine. I had a machine
		# that evidently was putting in a log4j jar file in a temporary folder, and when I shut down the services
		# the jar file was removed. 
		printf "WARNING - The file "$line" does not exist.\n"
    fi
	# Put some space in the output.
	printf "\n\n"
done < $1
